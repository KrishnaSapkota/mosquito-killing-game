#include<graphics.h>
#include<iostream.h>
#include<string.h>
#include<conio.h>
#include<dos.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#define CLICKED 0
#define NOT_CLICKED 1
start_screen();
void game();
int initmouse();
int showmp();
int hidemp();
int restrictmp(int,int,int,int);
int changempp(int,int);
int getmpos(int *,int *,int *);
int draw_window(int,int,int,int);
int click(int,int,int,int,char []);
void draw_racketunpushed(int,int);
void draw_racketpushed(int,int);
void draw_button(int,int,int,int,char []);
void pushed_button(int,int,int,int);
void unpushed_button(int,int,int,int);
void flying_mosquito(int,int);
void write_date(int,int);
void sure_quit();
void error();
void help();
int x,y,x1,y1,x2,y2,x11,y11,midx,midy,maxx,maxy,button,mx=15,my=13,mx1=20,my1=30;
int yb=5,xb=5,xb1=20,yb1=20,timetaken,timetaken1=0;
char t[10],timer[15];
union REGS i,o;
struct date d;
enum boolean{false,true};
enum timestatus{off,on};
timestatus timers=on;
boolean notkilled=true;
clock_t start, end,start1,end1,start2,end2;
void main()
{
	int gd=0,gm;
	initgraph(&gd,&gm,"d:\\tc\\bgi");
	maxx=getmaxx();
	maxy=getmaxy();
	midx=maxx/2;
	midy=maxy/2;
	initmouse();
	changempp(0,0);
	if((initmouse()!=0))
		{
			closegraph();
			restorecrtmode();
			printf(" ERROR...Mouse Driver Not Found.");
			getch();
			exit(1);
		}
	showmp();
	start_screen();
	closegraph();
	restorecrtmode();
}
start_screen()
{

	cleardevice();
	showmp();
	changempp(0,0);
	setviewport(0,0,maxx,maxy,1);
	showmp();
	changempp(0,0);
	restrictmp(0,0,maxx,maxy);
	draw_window(30,60,maxx-30,maxy-60);
	setcolor(YELLOW);
	outtextxy(33,68,"Mosquito Killing Game");
	setcolor(0);
	settextstyle(8,0,4);
	outtextxy(175,100,"W E L C O M E");
	settextstyle(0,0,0);
	outtextxy(195,385,"Programmed By Krishna Sapkota");
	draw_button(175,220,240,245,"HELP");
	draw_button(275,220,340,245,"GAME");
	draw_button(375,220,440,245,"EXIT");
	draw_button(195,300,425,335,"RESUME GAME");
	draw_button(maxx-60,62,maxx-40,80,"X");
	while(1)
	{
		getmpos(&button,&x,&y);
		showmp();
		if(click(275,220,340,245,"GAME")==0&&x>275&&x<340&&y>220&&y<245)
			{

				game();
			}
		 if(click(195,300,425,335,"RESUME GAME")==0&&x>195&&x<425&&y>300&&y<335)
			{
				if(notkilled==true)
					{
						error();
					}
				else
					{
						notkilled=true;
						timers=on;
						game();
					 }
			}

		if(click(375,220,440,245,"EXIT")==0&&x>375&&x<440&&y>220&&y<245)
			{

				 sure_quit();

			}
		if(click(175,220,240,245,"HELP")==0&&x>175&&x<240&&y>220&&y<245)
			{
				help();
			 }
		if(click(maxx-60,62,maxx-40,80,"X")==0&&x>maxx-60&&x<maxx-40&&y>62&&y<80)
			{
				sure_quit();
			}


	}
//	return 0;
}
void game()
{
	cleardevice();
	setcolor(0);
	setviewport(0,0,maxx,49,1);
	draw_window(0,0,maxx-12,maxy);
	setviewport(0,0,99,maxy,1);
	draw_window(0,0,maxx,maxy-8);
	setviewport(0,0,199,maxy,1);
	setcolor(YELLOW);
	setviewport(0,0,500,25,1);
	outtextxy(3,10,"Kill The Mosquito");
	outtextxy(255,10,"Right Click-Back");
	setviewport(0,80,48,90,0);
	setcolor(0);
	outtextxy(28,95,"Time");
	setcolor(0);
	outtextxy(16,145,"Seconds");
	outtextxy(12,175,"Remaning");

	gotoxy(30,3);
	setviewport(100,50,maxx,maxy,1);
	hidemp();
	start=clock();
	start1=clock();
	start2=clock();
	restrictmp(105,50,maxx-90,maxy-120);
	while(1)
	{
		setcolor(WHITE);
		rectangle(1,1,525,420);
		getmpos(&button,&x,&y);
		(button)==2?start_screen():getmpos(&button,&x,&y);
		draw_racketunpushed(x-100,y-35);
		getmpos(&button,&x,&y);
		if(notkilled)
		{
			 mx=random(maxx+(xb*1));
			 my=random(maxy+(yb*5));
			 flying_mosquito(mx,my);
			   /*flying_mosquito(mx1,my1);
			 mx1=random(maxx+(xb1*1));
			 my1=random(maxy+(yb1*5));*/
		 }
		 if(button==1)
		 {
				draw_racketpushed(x-100,y-35);
				delay(120);
		 }
		   if(mx>x-100&&mx<x-30&&my>y-35&&my<y+80&&button==1&&notkilled==true){notkilled=false;end=clock();timers=off;}
		   if(notkilled==false&&timetaken1<=10)
		   {

			setviewport(0,0,maxx,maxy,1);
			setcolor(0);
			outtextxy(100,40,"You Killed,");
			outtextxy(200,40,"It took   Seconds");
			timetaken=(end-start)/CLK_TCK;
			itoa(timetaken,t,10);
			outtextxy(260,40,t);
			setviewport(100,50,maxx,maxy,1);
		   }
		if((end2-start2)/CLK_TCK>0&&timers==on)
		{       //timetaken1++;
			timetaken1=11-(end1-start1)/CLK_TCK;
			setviewport(0,80,48,90,0);
			itoa(timetaken1,timer,10);
			setcolor(0);
			settextstyle(3,0,4);
			outtextxy(30,110,timer);
			delay(35);
			setcolor(7);
			outtextxy(30,110,timer);
			start2=clock();
		}
		end2=clock();
		end1=clock();
		timetaken1=(end1-start1)/CLK_TCK;
		if(timetaken1>10&&notkilled==true)
		{
			setviewport(0,0,maxx,maxy,1);
			settextstyle(0,0,0);
			setcolor(0);
			outtextxy(350,40,"You could not Kill it.");
			setviewport(100,50,maxx,maxy,1);
			notkilled=false;
			timers=off;
		 }
		 if(notkilled==false)
		 {
			setfillstyle(1,4);
			bar(mx,my,mx+6,my+4);
			bar(mx+2,my,mx+4,my-5);
			bar(mx+2,my+6,mx+4,my+10);
		   }
		if(notkilled)
		{
			setviewport(0,0,maxx,maxy,1);
			settextstyle(0,0,0);
			setcolor(0);
			outtextxy(200,30,"Kill the mosquito with in 10 Seconds");
			setviewport(100,50,maxx,maxy,1);
		}
	   }

}
void flying_mosquito(int mx,int my)
{
		setfillstyle(1,4);
		bar(mx,my,mx+6,my+4);
		bar(mx+2,my,mx+4,my-5);
		bar(mx+2,my+6,mx+4,my+10);
		delay(35);
		setfillstyle(1,0);
		bar(mx,my,mx+6,my+4);
		bar(mx+2,my,mx+4,my-5);
		bar(mx+2,my+6,mx+4,my+10);
		mx=mx+(xb*1);
		my=my+(yb*5);
		if(mx>500||mx<3)
		{
			xb *= -1;
			sound(700);
			delay(40);
			nosound();
		}
		if(my>400||my<3)
		{
			yb *= -1;
			sound(300);
			delay(40);
			nosound();
		}
		if(mx1>500||mx1<3)
		{
			xb1*= -1;
			sound(700);
			delay(40);
			nosound();
		}
		if(my1>400||my1<3)
		{
			yb1 *= -1;
			sound(300);
			delay(40);
			nosound();
		}

}

void sure_quit()
{

	  showmp();
	  changempp(0,0);
	  draw_window(250,230,420,340);
	  setcolor(14);
	  outtextxy(252,233,"?");
	  setcolor(1);
	  outtextxy(285,263,"Are you sure");
	  outtextxy(315,275,"Quit?");
	  draw_button(275,300,325,330,"Yes");
	  draw_button(350,300,400,330,"No");
	  while(1)
	  {
		getmpos(&button,&x,&y);
		showmp();
		if(click(350,300,400,330,"No")==0&&x>350&&x<400&&y>300&&y<330)
			{
				start_screen();

			 }

			if(click(275,300,325,330,"Yes")==0&&x>275&&x<325&&y>300&&y<330)
			 {
				initmouse();
				closegraph();
				restorecrtmode();
				exit(0);
			 }
	  }
}
draw_window(int x1,int y1,int x2,int y2)
{
	setlinestyle(0,1,1);
	setfillstyle(1,7);
	bar(x1,y1,x2,y2);
	setcolor(WHITE);
	line(x1,y1,x2,y1);
	line(x1,y1,x1,y2);
	setcolor(0);
	line(x1,y2,x2,y2);
	line(x2,y1,x2,y2);
	setfillstyle(1,1);
	bar(x1+1,y1+1,x2-1,y1+20);
	write_date(x2-100,y1-200);
	return 0;
}
initmouse()
{
	i.x.ax=0;
	int86(0x33,&i,&o);
	return(i.x.ax);
}
showmp()
{
	i.x.ax=1;
	int86(0x33,&i,&o);
	return(0);
}
hidemp()
{
	i.x.ax=2;
	int86(0x33,&i,&o);
	return(0);
}
restrictmp(int x1,int y1,int x2,int y2)
{
	i.x.ax=7;
	i.x.cx=x1;
	i.x.dx=x2;
	int86(0x33,&i,&o);
	i.x.ax=8;
	i.x.cx=y1;
	i.x.dx=y2;
	return(0);
}
changempp(int x11,int y11)
{
	i.x.ax=4;
	int86(0x33,&i,&o);
	i.x.cx=x11;
	i.x.dx=y11;
	return(0);
}
getmpos(int *button,int *x,int *y)
{
	 i.x.ax=3;
	 int86(0x33,&i,&o);
	 *button=o.x.bx;
	 *x=o.x.cx;
	 *y=o.x.dx;
	 return 0;
}

void draw_racketunpushed(int x,int y)
{
	setcolor(WHITE);
	setfillstyle(7,WHITE);
	bar(x,y,x+70,y+80);
	setfillstyle(1,RED);
	bar(x+40,y+60,x+30,y+130);
	delay(100);
	setfillstyle(1,BLACK);
	bar(x,y,x+70,y+80);
	bar(x+40,y+60,x+30,y+130);

}
void draw_racketpushed(int x,int y)
{
	setcolor(WHITE);
	setfillstyle(7,WHITE);
	bar(x+10,y+5,x+60,y+70);
	setfillstyle(1,RED);
	bar(x+35,y+65,x+40,y+125);
	delay(100);
	setfillstyle(7,BLACK);
	bar(x+10,y+5,x+60,y+70);
	bar(x+35,y+65,x+40,y+125);
}
void draw_button(int x1,int y1,int x2,int y2 ,char s[20])
{
	int t1,t2,k=0,i;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(s);i++)
	k=k+4;
	t1=(x2-x1)/2+x1-k;
	t2=(y2-y1)/2+y1;
	unpushed_button(x1,y1,x2,y2);
	settextstyle(0,0,0);
	outtextxy(t1,t2,s);
}
void unpushed_button(int x1,int y1,int x2,int y2)
{
	setlinestyle(0,1,1);
	setfillstyle(1,7);
	bar(x1,y1,x2,y2);
	setcolor(WHITE);
	line(x1,y1,x2,y1);
	line(x1,y1,x1,y2);
	setcolor(BLACK);
	line(x2,y1,x2,y2);
	line(x1,y2,x2,y2);
}
void pushed_button(int x1,int y1,int x2,int y2)
{
	setlinestyle(0,1,1);
	setfillstyle(1,7);
	bar(x1,y1,x2,y2);
	setcolor(BLACK);
	line(x1,y1,x2,y1);
	line(x1,y1,x1,y2);
	setcolor(WHITE);
	line(x2,y1,x2,y2);
	line(x1,y2,x2,y2);
}
int click(int x1,int y1,int x2,int y2,char s[20])
{
	int t1,t2,i,k=0;
	setlinestyle(0,1,1);
	for(i=0;i<strlen(s);i++)
	k=k+4;
	t1=(x2-x1)/2+x1-k;
	t2=(y2-y1)/2+y1;
	getmpos(&button,&x,&y);
	if( (x>x1 && x<x2) && (y>y1 && y<y2) && (button==1))
	{
		  hidemp();
		  pushed_button(x1,y1,x2,y2);
		  setcolor(0);
		  settextstyle(0,0,0);
		  outtextxy(t1+2,t2+2,s);
		  showmp();
		  while((button==1))
		  getmpos(&button,&x,&y);
		  hidemp();
		  unpushed_button(x1,y1,x2,y2);
		  settextstyle(0,0,0);
		  outtextxy(t1,t2,s);
		  for(i=0;i<500;i=i+50)
		  {
		delay(15);
		sound(i+200);
		  }
		 nosound();
		 return CLICKED;
	}
	else
	 {
		return NOT_CLICKED;
	 }

}
void write_date(int x2,int y1)
{
	int a,b,c;
	getdate(&d);
	char d1[10],d2[10],d3[10];
	textcolor(WHITE);
	setcolor(YELLOW);
	a=d.da_year;
	b=d.da_day;
	c=d.da_mon;
	itoa(a,d1, 10);
	itoa(b,d2, 10);
	itoa(c,d3, 10);
	outtextxy(x2-20,y1+210,d3);
	outtextxy(x2-10,y1+210,"/");
	outtextxy(x2,y1+210,d2);
	outtextxy(x2+12,y1+210,"/");
	outtextxy(x2+24,y1+210,d1);
	setcolor(BLACK);
}
void error()
{
	  showmp();
	  changempp(0,0);
	  draw_window(250,230,420,340);
	  setcolor(14);
	  outtextxy(252,239,"Error.");
	  setcolor(1);
	  outtextxy(265,263,"No game to resume!");
	  draw_button(295,300,370,330,"OK");
	  while(1)
		{
			getmpos(&button,&x,&y);
			showmp();
			if(click(295,300,370,330,"OK")==0&&x>295&&x<370&&y>300&&y<330)
				{
					start_screen();
				}
		}
}
void help()
{
	cleardevice();
	showmp();
	changempp(0,0);
	draw_window(100,100,540,425);
	setcolor(14);
	outtextxy(105,105,"HELP");
	setfillstyle(1,WHITE);
	bar(150,150,490,380);
	setcolor(WHITE);
	line(150,150,490,150);
	line(150,150,150,380);
	setcolor(BLACK);
	line(150,150,490,150);
	line(150,150,150,380);
	outtextxy(155,155,"This is a small game in which you have to");
	outtextxy(155,165,"kill the mosquito flying on the screen by");
	outtextxy(155,175,"clicking the left mouse button with in the");
	outtextxy(155,185,"given time.");
	outtextxy(167,195,"  You have to set the racket position on");
	outtextxy(155,205,"the body of  the  mosquito to kill it, do");
	outtextxy(155,215,"right click to  go to  the starting  main  ");
	outtextxy(155,225,"menu,when you are in game screen. ");
	outtextxy(220,340,"Wait For Next Version....");
	outtextxy(157,290,"Kill the mosquitos and beware of malaria!");
	outtextxy(200,370,"(C) Copyright krishna sapkota");
	draw_button(285,390,340,410,"OK");
	draw_button(510,102,530,120,"X");
	while(1)
	{

		showmp();
		getmpos(&button,&x,&y);
		if(click(285,390,340,410,"OK")==0&&x>285&&x<340&&y>390&&y<410)
		{
			start_screen();
		}
		 if(click(510,102,530,120,"X")==0&&x>510&&x<530&&y>102&&y<120)
		 {
			sure_quit();
		}


	}
}














































